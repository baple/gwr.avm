# GWR for AVM

## 开发环境搭建

### docker启动Rstudio

```
docker run --name rstudio_geo -p 8787:8787 -d --restart=always -v $(pwd):/home/$USER/foo  -e USER=$USER -e USERID=$UID  rocker/geospatial

```

#### docker-compose

Rstudio与MySQL的docker-compose一键部署

```
export UID
docker-compose up -d 
```

访问地址[http://127.0.0.1:8788/](http://127.0.0.1:8788/)进入Rstudio开发环境，进行R包的开发与测试。
