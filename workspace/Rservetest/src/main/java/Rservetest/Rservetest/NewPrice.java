package Rservetest.Rservetest;

import java.util.Properties;

import org.math.R.RserveSession;
import org.math.R.RserverConf;
import org.math.R.Rsession;
import org.math.R.Rsession.RException;

public class NewPrice {
	public static void main(String args[]) throws RException {
		//远程R服务器应该有权限读取数据库!!!!并且安装好GDAL,proj4和GEOS开源GIS库，有权限安装R包
		//远程R服务器运行Rserve::Rserve(args=' --no-restore --no-save --slave --RS-enable-remote',quote=F)
		RserverConf rconf = new RserverConf("10.10.10.56", 6311, "", "", new Properties());		
		Rsession s = null;
		s = RserveSession.newRemoteInstance(System.out, rconf);
		// 存放新楼盘插值脚本的目录，脚本需要上传，setwd设置工作目录
		s.voidEval("setwd('/home/wangchl/newprice_model')");		
		//加载脚本函数
		s.voidEval("source('batches.R',encoding='UTF-8')");
		// 建立输出结果目录
		s.voidEval("dir.create('./result')");
		// 批量执行函数
		s.voidEval("batches(host = '10.11.10.34', port = 3307, user = 'datatestuser',"
				+ " password = 'cityhousedata_db_2017', startmon = '201109', endmon = '201110',"
				+ " resol = 500, outpath = './result')");
		s.close();
//		参数解释：
//	    host —— 主机地址
//	    port —— 端口
//	    user —— 用户名
//	    password —— 密码
//	    starmon —— 开始年月
//	    endmon —— 结束年月
//	    resol —— 分辨率(m) 
//	    outpath —— 文件保存路径
	}
	
}
