package Rservetest.Rservetest;

import java.util.Properties;

import org.math.R.RserveSession;
import org.math.R.RserverConf;
import org.math.R.Rsession;
import org.math.R.Rsession.RException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws RException
    {
    	RserverConf rconf = new RserverConf("10.16.1.161", 6311, "", "", new Properties());		
		Rsession s = null;
		s = RserveSession.newRemoteInstance(System.out, rconf);
		s.voidEval("rmse <- function(error)" + 
				"{sqrt(mean(error^2,na.rm=T))}");
		s.voidEval("sqrt(NA)");
		s.close();
    }
}
